#!/usr/bin/env python
# AUTHOR:   Shane Gordon
# FILE:     offlineimap.py
# ROLE:     TODO (some explanation)
# CREATED:  2015-01-13 11:07:19
# MODIFIED: 2015-03-22 10:59:13

import time
import subprocess
from glob import glob

while True:
        # Make sure there aren't any lock files before beginning
        if not glob.glob('/home/sgordon/.offlineimap/*.lock'):
                print "\n no lockfiles found. \n"
                subprocess.call("offlineimap -c ~/.mutt/offlineimaprc",
                                shell=True)
                time.sleep(20)  # Wait a little before looping
        else:
                print "\n lockfiles found. \n"
                time.sleep(20)
